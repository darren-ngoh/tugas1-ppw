from django.urls import path
from .views import signUp, loginUser, logoutUser
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', signUp, name="signUp"),
    path('log/', loginUser, name="loginUser"),
    path('logout/', logoutUser, name="logoutUser"),
]