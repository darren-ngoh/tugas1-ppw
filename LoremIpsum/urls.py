"""LoremIpsum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import render
from Barang.urls import urlpatterns as barang_urls
from Kupon.urls import urlpatterns as kupon_urls
from Transaksi.urls import urlpatterns as transaksi_urls
from Ulasan.urls import urlpatterns as ulasan_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('coupon/', include('Kupon.urls')),
    path('accounts/', include('allauth.urls')),
    path('login/', include('Login.urls')),
]

urlpatterns += barang_urls
urlpatterns += kupon_urls
urlpatterns += transaksi_urls
urlpatterns += ulasan_urls