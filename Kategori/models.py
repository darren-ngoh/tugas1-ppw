from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=50)

def find_all_categories():
    items = Category.objects.all()
    return items
