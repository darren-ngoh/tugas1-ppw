from django.shortcuts import render, redirect
from . import models
from django.urls import reverse
from Ulasan.models import find_reviews_by_itemid
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import json

# Create your views here.
def index(request):
    ctx = {}
    page = request.GET.get('page')
    if page is None:
        page = 1
    items = models.find_all_items(page, 10)
    ctx['items'] = items
    return render(request, 'index.html', ctx)

def search(request):
    ctx = {}
    query = request.GET.get('q')
    if query is None:
        return redirect('/')
    items = models.search_item(query)
    ctx['items'] = items
    ctx['count'] = len(items)
    ctx['query'] = query
    return render(request, 'search_result.html', ctx)

def item_detail(request, item_id: int):
    ctx = {}
    try:
        item = models.find_item_by_id(item_id)
        ctx['item'] = item
        ctx['review_url'] = reverse('review', kwargs={'id':item.id})
        review = find_reviews_by_itemid(item.id)
        review_list = []
        for r in review:
            can_delete = False
            if request.user.username == r.nama_pengulas:
                can_delete = True
            review_list.append({"nama_pengulas":r.nama_pengulas, "bintang":r.bintang, "barang":r.barang, "pesan":r.pesan, "tanggal":r.tanggal, "can_delete":can_delete})
        ctx['reviews'] = review_list
        return render(request, 'detail.html', ctx)
    except Exception as e:
        messages.add_message(request, messages.ERROR, str(e))
        return redirect('/')

def category(request, category: str):
    ctx = {}
    try:
        items = models.find_items_by_category(category)
        ctx['items'] = items
        ctx['count'] = len(items)
        ctx['category'] = category
        return render(request, 'category.html', ctx)
    except Exception as e:
        messages.add_message(request, messages.ERROR, str(e))
        return redirect('/')

@csrf_exempt
def add_item_to_cart(request):
    if request.method == 'POST':
        try:
            req = json.loads(request.body)
            if request.user.id is None:
                return JsonResponse({'error': 'unauthenticated'}, status= 401 ,safe=False)
            user = User.objects.get(pk=request.user.id)
            cart = models.Cart.objects.filter(user=user).order_by('-id').first()
            if cart is None:
                cart = models.Cart(user=user)
                cart.save()

            item = models.Barang.objects.get(pk=req['item_id'])
            existing_cart = models.CartItem.objects.filter(cart=cart, item=item)
            if len(existing_cart) > 0:
                return JsonResponse(req, safe=False)
            cart_item = models.CartItem(cart=cart, item=item)
            cart_item.save()
            return JsonResponse(req, safe=False)

        except Exception as e:
            return JsonResponse({'error': str(e)}, status= 500 ,safe=False)

    return JsonResponse({'error': 'method not allowed'}, status= 405 ,safe=False)

@csrf_exempt
def find_current_user_cart(request):
    if request.method == 'GET':
        try:
            if request.user.id is None:
                return JsonResponse({'error': 'unauthenticated'}, status= 401 ,safe=False)

            user = User.objects.get(pk=request.user.id)
            user_cart = models.Cart.objects.filter(user=user).order_by('-id').first()
            if user_cart is None:
                user_cart = models.Cart(user=user)
                user_cart.save()


            cart_items = models.CartItem.objects.filter(cart=user_cart)
            res = {'data': []}
            for item in cart_items:
                res['data'].append({'id': item.id, 'name': item.item.name, 'price': item.item.price, 'image_url': item.item.image_url})

            return JsonResponse(res, status= 200 ,safe=False)


        except Exception as e:
            return JsonResponse({'error': str(e)}, status= 500 ,safe=False)

    return JsonResponse({'error': 'method not allowed'}, status= 405 ,safe=False)

@csrf_exempt
def delete_item_from_cart(request, item_id:int):
    if request.method == 'POST':
        try:
            if request.user.id is None:
                return JsonResponse({'error': 'unauthenticated'}, status= 401 ,safe=False)

            cart_item = models.CartItem(pk=item_id)
            cart_item.delete()

            return JsonResponse({'item_id': item_id}, status= 200 ,safe=False)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status= 500 ,safe=False)
    
    return JsonResponse({'error': 'method not allowed'}, status= 405 ,safe=False)
