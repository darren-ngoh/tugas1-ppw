from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('search', views.search),
    path('item/<int:item_id>', views.item_detail),
    path('category/<str:category>', views.category),

    # Cart API Endpoints
    path('api/cart/add', views.add_item_to_cart),
    path('api/cart', views.find_current_user_cart),
    path('api/cart/<int:item_id>/delete', views.delete_item_from_cart)
    
]