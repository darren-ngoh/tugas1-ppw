from django.test import TestCase
from Barang import models
from Barang.models import Cart, CartItem, Barang
from django.contrib.auth.models import User

# Create your tests here.
class BarangModelTest(TestCase):
    def test_create_item(self):
        models.create_item(models.Barang(name="New item", price=20000, description="hehe", buy_count=0))
        self.assertEqual(models.Barang.objects.all().count(), 1)

    def test_find_all_items(self):
        for i in range(10):
            models.create_item(models.Barang(name="New item {}".format(i), price=20000, description="hehe", buy_count=0))
        res = models.find_all_items(1, 4)
        self.assertEqual(len(res), 4)

    def test_find_item_by_id(self):
        item = models.Barang(name="New item", price=20000, description="hehe", buy_count=0)
        models.create_item(item)
        res = models.find_item_by_id(1)
        self.assertEqual(res, item)

    def test_find_best_seller_items(self):
        items = []
        for i in range(8):
            item = models.Barang(name="New item {}".format(i), price=20000, description="hehe", buy_count=i)
            item.save()
            items.append(item)

        res = models.find_best_seller_items()
        self.assertEqual(res[0], items[-1])


class CartModelTest(TestCase):
    def test_can_add_cart(self):
        user = User()
        user.save()
        cart = Cart(user=user)
        cart.save()
        self.assertEqual(Cart.objects.count(), 1)

    def test_can_add_cartitem(self):
        user = User()
        user.save()
        cart = Cart(user=user)
        cart.save()
        models.create_item(models.Barang(name="New item", price=20000, description="hehe", buy_count=0))
        barang = Barang.objects.get(pk=1)
        cart_item = CartItem(cart=cart, item=barang)
        cart_item.save()
        self.assertEqual(CartItem.objects.count(), 1)