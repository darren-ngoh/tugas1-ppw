from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from Kategori.models import Category
from django.contrib.auth.models import User

# Create your models here.
class Barang(models.Model):
    name = models.CharField(max_length=50)
    price = models.BigIntegerField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    description = models.TextField()
    buy_count = models.IntegerField()
    image_url = models.CharField(max_length=100)

def find_all_items(page: int, size: int):
    item_list = Barang.objects.all()
    paginator = Paginator(item_list, size)
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    return items

def find_item_by_id(item_id: int):
    item = Barang.objects.get(pk=item_id)
    return item

def find_best_seller_items():
    items = Barang.objects.order_by('-buy_count')[:8]
    return items

def create_item(req: Barang):
    req.save()

def search_item(query: str):
    items = Barang.objects.filter(name__contains=query)
    return items

def find_items_by_category(category_name: str):
    category = Category.objects.get(name=category_name)
    items = Barang.objects.filter(category=category.id)
    return items

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    item = models.ForeignKey(Barang, on_delete=models.CASCADE)