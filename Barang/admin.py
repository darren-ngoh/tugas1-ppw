from django.contrib import admin
from .models import Barang, Cart, CartItem

# Register your models here.
admin.site.register(Barang)
admin.site.register(Cart)
admin.site.register(CartItem)