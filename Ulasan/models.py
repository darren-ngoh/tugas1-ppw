from django.db import models
from Barang.models import Barang

# Create your models here.
class Review(models.Model):
    nama_pengulas = models.CharField(max_length=50)
    bintang = models.IntegerField()
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    pesan = models.TextField()
    tanggal = models.DateField(auto_now=True)

def find_reviews_by_itemid(itemId: int):
    item = Barang.objects.get(pk = itemId)
    review = Review.objects.filter(barang = item)
    return review