from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'Story5'

urlpatterns = [
    path('item/<int:id>/review', views.create_review, name='review'),
    path('api/review', views.ajax_review),
    path('api/review/<int:id>', views.find_review_by_id),
    path('api/delete_review/<int:id>', views.delete_review_by_id)
]