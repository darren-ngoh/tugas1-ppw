from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from . import forms
from Barang.models import find_item_by_id
from .models import Review, Barang
import json

# Create your views here.
def create_review(request, id):
    if request.method == "POST":
        nama_pengulas = request.POST.get('nama_pengulas')
        star = request.POST.get('bintang')
        pesan = request.POST.get('pesan')
        barang = find_item_by_id(id)
        ulasan = Review(nama_pengulas=nama_pengulas, bintang=star, pesan=pesan, barang=barang)
        ulasan.save()
        return redirect(request.META.get('HTTP_REFERER', '/'))

@csrf_exempt
def ajax_review(request):
    review = json.loads(request.body)
    try:
        barang = Barang.objects.get(pk=int(review['barang_id']))
        review_object = Review(bintang=int(review['bintang']), pesan=review['pesan'], barang=barang, nama_pengulas=request.user.username)
        review_object.save()
        return JsonResponse(review, safe=False)
    except Exception as e:
        return JsonResponse({"error":str(e)}, status=500, safe=False)

def find_review_by_id(request, id):
    can_delete = False
    review = Review.objects.order_by("-id")
    json_review = {
        "data":[]
    }
    for r in review:
        if request.user.username == r.nama_pengulas:
            can_delete = True
        json_review["data"].append({'pesan':r.pesan, 'bintang':r.bintang, 'barang_id':r.barang.id, 'nama_pengulas':r.nama_pengulas, 'tanggal':r.tanggal, 'review_id':r.id, 'can_delete':can_delete})
    return JsonResponse(json_review, safe=False)

@csrf_exempt
def delete_review_by_id(request, id):
    Review.objects.get(pk=id).delete()
    return HttpResponse()