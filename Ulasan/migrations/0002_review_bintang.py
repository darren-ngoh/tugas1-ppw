# Generated by Django 3.0.2 on 2020-03-06 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Ulasan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='bintang',
            field=models.IntegerField(default=5),
            preserve_default=False,
        ),
    ]
