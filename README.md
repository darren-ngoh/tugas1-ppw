# Tugas1-PPW
## Anggota Kelompok
1. Shahnaz Estee Laseidha (1606883146)
2. Darren Ngoh (1906350723)
3. Michelle Shie (1906293165)
4. Samuel Mulatua (1906308305)
5. Nikita Jacey Natania (1906353933)

## Status Pipeline
![pipeline](https://gitlab.com/darrenngoh/tugas1-ppw/badges/master/pipeline.svg)

## Status Code Coverage
![coverage](https://gitlab.com/darrenngoh/tugas1-ppw/badges/master/coverage.svg)

## Link Heroku
[https://ppw-online-shop.herokuapp.com/](https://ppw-online-shop.herokuapp.com/)