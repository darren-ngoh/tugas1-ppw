from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Kupon
# Create your views here.

def join(request):
    cari = request.GET.get('search')
    if cari is not None:
        searched = Kupon.objects.filter(kode__icontains = cari)
        dictio_search = {
            'searched': searched,
        }
        return render(request, 'coupon.html',dictio_search )
    data_kupon = Kupon.objects.all()
    dictio = {
        'data_kupon' : data_kupon,
    }
    return render(request, 'coupon.html', dictio )

def cari(request):
    kupon = Kupon.objects.all()
    data_kupon = []
    for i in kupon:
        data_kupon.append({
            'kode': i.kode,
            'persen': i.persen,
            'min': i.min,
            'exp': i.exp,
            'active': i.active
        })
    json_data = {"data_kupon": data_kupon}
    return JsonResponse(json_data, safe=False)
