from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class Kupon(models.Model):
        kode= models.CharField(max_length= 15)
        exp = models.DateField()
        min = models.IntegerField()
        persen = models.IntegerField(validators = [MinValueValidator(0), MaxValueValidator(100)])
        active = models.BooleanField(default = True)

        def __str__(self):
            return self.kode



