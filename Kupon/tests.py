from django.test import TestCase, Client
from django.urls import reverse, resolve
from Kupon import models
from django.utils import timezone
import datetime

class KuponTest(TestCase):
    def test_kupon_model_add_kupon(self):
        models.Kupon.objects.create(kode = 'test', min=100000, persen=50, exp=timezone.now(), active=True)
        hitungjumlah = models.Kupon.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_kupon_model_check_name(self):
        models.Kupon.objects.create(kode = 'test', min=100000, persen=50, exp=timezone.now(), active=True)
        var_name = models.Kupon.objects.get(kode='test')
        self.assertEqual(str(var_name), 'test')
