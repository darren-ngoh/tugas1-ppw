from django import forms

class KuponForm(forms.Form):
    kupon = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Search Coupon...',
            'type' : 'text',
            'required' : True,
    }))