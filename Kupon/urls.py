from django.urls import path
from .views import join, cari

appname = 'Kupon'

urlpatterns = [
    path('', join),
    path('cari', cari, name='cari')
]
