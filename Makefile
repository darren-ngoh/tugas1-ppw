
run:
	python manage.py runserver

run3:
	python3 manage.py runserver

test:
	coverage run --include='Barang/*' manage.py test
	coverage report -m

test3:
	python3 manage.py test

migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate