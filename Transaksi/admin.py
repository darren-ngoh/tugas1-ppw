from django.contrib import admin
from .models import Transaksi, Transaction, TransactionItem

# Register your models here.
admin.site.register(Transaksi)
admin.site.register(Transaction)
admin.site.register(TransactionItem)