from django.urls import path
from . import views


urlpatterns = [
    # path('checkout/<int:product_id>', views.checkout), DEPRECATED
    path('checkout', views.checkout_window),
    path('api/checkout', views.create_transaction),
    path('api/transactions', views.get_user_transaction_history)
]