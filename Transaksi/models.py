from django.db import models
from Barang.models import Barang
from Kupon.models import Kupon
from django.contrib.auth.models import User
# Create your models here.

# deprecated
class Transaksi(models.Model):
    nama_pembeli = models.CharField(max_length=100)
    quantity = models.IntegerField()
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    kupon = models.ForeignKey(Kupon, on_delete=models.CASCADE)
    tanggal_beli = models.DateTimeField(auto_now_add=True)

class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    coupon = models.ForeignKey(Kupon, null=True, on_delete=models.CASCADE)

class TransactionItem(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    item = models.ForeignKey(Barang, on_delete=models.CASCADE)
    quantity = models.IntegerField()