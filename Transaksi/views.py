from django.shortcuts import render, redirect
from Barang import models as barang_models
from Kupon import models as kupon_models
from django.contrib import messages
from .models import Transaksi, Transaction, TransactionItem
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import json

# deprecated
def checkout(request, product_id: int):
    ctx = {}
    if request.method == 'POST':
        name = request.POST.get('name')
        coupon = request.POST.get('coupon')
        quantity = request.POST.get('quantity')
        item_id = request.POST.get('id')
        try:
            item = barang_models.find_item_by_id(item_id)
            kupon = kupon_models.Kupon.objects.get(kode=coupon)
            obj = Transaksi(nama_pembeli = name, quantity=quantity, barang=item, kupon=kupon)
            obj.save()
            return redirect('/')
        except Exception as e:
            messages.add_message(request, messages.ERROR, str(e))
            return redirect('/')
    try:
        item = barang_models.find_item_by_id(product_id)
        ctx['item'] = item
        return render(request, 'transactions.html', ctx)
    except Exception as e:
        messages.add_message(request, messages.ERROR, str(e))
        return redirect('/')


def cari(request):
    kupon = kupon_models.Kupon.objects.all()
    data_kupon = []
    for i in kupon:
        data_kupon.append({
            'kode': i.kode,
            'persen': i.persen,
            'min': i.min,
            'exp': i.exp,
            'active': i.active
        })
    json_data = {"data_kupon": data_kupon}
    return JsonResponse(json_data, safe=False)


@login_required
def checkout_window(request):
    try:
        ctx = {}
        user = User.objects.get(pk=request.user.id)
        user_cart = barang_models.Cart.objects.filter(user=user).order_by('-id').first()
        cart_items = barang_models.CartItem.objects.filter(cart=user_cart)
        ctx['items'] = cart_items
        if len(cart_items) == 0:
            messages.add_message(request, messages.ERROR, 'Cart is empty')
            return redirect('/')

        # count subtotal
        subtotal = 0
        for item in cart_items:
            subtotal += item.item.price

        ctx['subtotal'] = subtotal
        return render(request, 'transactions.html', ctx)
    except Exception as e:
        messages.add_message(request, messages.ERROR, str(e))
        return redirect('/')


@csrf_exempt
def create_transaction(request):
    req = json.loads(request.body)
    if request.user.id is None:
        return JsonResponse({'error': 'unauthenticated'}, status= 401 ,safe=False)
    
    if request.method == 'POST':
        try:
            kupon = None
            if req['coupon'] != "":
                kupon = kupon_models.Kupon.objects.get(kode=req['coupon'])
                if kupon is None:
                    return JsonResponse({"error":"coupon not found"}, safe=False, status=404)
            user = User.objects.get(pk=request.user.id)
            trx = Transaction(user=user, coupon=kupon)
            trx.save()

            for i in req['items']:
                item = barang_models.Barang.objects.get(pk=int(i['item_id']))
                trx_item = TransactionItem(transaction=trx, item=item, quantity=int(i['quantity']))
                if trx_item.quantity == 0:
                    return JsonResponse({"error":"invalid input"}, safe=False, status=500)
                trx_item.save()

            # delete current user cart
            user_cart = barang_models.Cart.objects.get(user=user)
            user_cart.delete()

            return JsonResponse(req, safe=False, status=200)

        except Exception as e:
            return JsonResponse({"error":str(e)}, safe=False, status=500)

    return JsonResponse({"error":"method not allowed"}, safe=False, status=405)

@csrf_exempt
def get_user_transaction_history(request):
    if request.user.id is None:
        return JsonResponse({'error': 'unauthenticated'}, status= 401 ,safe=False)

    res = []
    user = User.objects.get(pk=request.user.id)
    history = Transaction.objects.filter(user=user)
    for h in history:
        history_dict = []
        for i in TransactionItem.objects.filter(transaction=h):
            item = {'name': i.item.name, 'qty': i.quantity}
            history_dict.append(item)
        res.append({"items": history_dict, "trxId": h.id})

    return JsonResponse({"data": res}, safe=False, status=200)
